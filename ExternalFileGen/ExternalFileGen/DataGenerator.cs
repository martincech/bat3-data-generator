﻿
using System;
using System.Collections.Generic;
using System.Globalization;

namespace ExternalFileGen
{
   class DataGenerator
   {
      private readonly string dateTimeFormat;
      static readonly Random random = new Random();
      private NumberFormatInfo numberFormatInfo;
      private readonly Dictionary<Sensor, List<string>> sensors = new Dictionary<Sensor, List<string>>();
      public DataGenerator(string separator, Dictionary<Sensor, int> sensorsCount, string dateTimeFormat = "s")
      {
         numberFormatInfo = new NumberFormatInfo { NumberDecimalSeparator = separator };
         foreach (var s in sensorsCount)
         {
            var senses = new List<string>();
            var prefix = NameForSensor(s.Key);
            for (int i = 0; i < s.Value; i++)
            {
               senses.Add($"{prefix}_{(i + 1):00}");
            }
            sensors.Add(s.Key, senses);
         }

         this.dateTimeFormat = dateTimeFormat;
      }

      public Dictionary<Sensor, IEnumerable<String>> Generate()
      {
         var result = new Dictionary<Sensor, IEnumerable<String>>();
         foreach (var sensor in sensors)
         {
            var rows = new List<String>();
            foreach (var sens in sensor.Value)
            {
               rows.Add(GenerateRow(sensor.Key, sens));
            }
            result.Add(sensor.Key, rows);
         }
         return result;
      }
      private static string NameForSensor(Sensor sensor)
      {
         switch (sensor)
         {
            case Sensor.Temperature:
               return "Temp";
            case Sensor.Humidity:
               return "Hum";
            case Sensor.Co2:
               return "Co2";
            case Sensor.BirdWeight:
               return "BW";
         }
         return "";
      }

      private string GenerateRow(Sensor sensor, string uid)
      {
         var row = uid + ";" + DateTime.Now.ToString(dateTimeFormat) + ";";
         double number;

         switch (sensor)
         {
            case Sensor.Temperature:
               number = random.NextDouble() * 50;
               break;
            case Sensor.Humidity:
               number = 50 + random.NextDouble() * 50;
               break;
            case Sensor.Co2:
               number = 400 + random.NextDouble() * 2000;
               break;
            case Sensor.BirdWeight:
               number = 50 + random.NextDouble() * 2000;
               break;
            default:
               number = 50 + random.NextDouble() * 100;
               break;
         }

         row += number.ToString(numberFormatInfo);
         return row;
      }


   }
}
