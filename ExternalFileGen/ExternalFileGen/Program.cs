﻿using CommandLine;

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace ExternalFileGen
{
   class Program
   {
      static void Main(string[] args)
      {
         var options = new Options();
         var parseResult = Parser.Default.ParseArguments<Options>(args);
         using (var cancelationSource = new CancellationTokenSource())
         {
            if (!parseResult.MapResult(
            (Options opts) => Execute(opts, cancelationSource.Token),
            _ => MakeError()))
            {
               return;
            }

            Console.WriteLine("Press enter to exit");

            while (Console.ReadKey().Key != ConsoleKey.Enter)
            {
            }
            Console.WriteLine("Closing");
            cancelationSource.Cancel();
            cancelationSource.Token.WaitHandle.WaitOne(1000);
         }
      }

      private static bool MakeError()
      {
         return false;
      }

      public static bool Execute(Options options, CancellationToken cancelToken)
      {
         var task = Task.Factory.StartNew((a) =>
         {
            var sensors = new Dictionary<Sensor, int> {
            {Sensor.Temperature, options.SensorsTemp },
            {Sensor.Humidity, options.SensorsHumidity },
            {Sensor.Co2, options.SensorsCo2 },
            {Sensor.BirdWeight, options.SensorsBirdWeight }};

            var generator = new DataGenerator(options.Separator, sensors, options.DateTimeFormat);
            if (!Directory.Exists(options.Path)) Directory.CreateDirectory(options.Path);

            var counter = 0;
            while (!cancelToken.IsCancellationRequested)
            {
               var generatedData = generator.Generate();
               foreach (var generated in generatedData)
               {
                  var writen = false;
                  while (!writen && !cancelToken.IsCancellationRequested)
                  {
                     try
                     {
                        File.AppendAllLines(options.Path + generated.Key + ".csv", generated.Value);
                        writen = true;
                     }
                     catch (Exception)
                     {
                        writen = false;
                     }
                  }
               }
               Console.Write(".");
               counter++;
               if (counter >= 100)
               {
                  Console.Clear();
                  counter = 0;
               }
               Thread.Sleep(options.Period);
            }
         }, null, cancelToken, TaskCreationOptions.LongRunning, TaskScheduler.Current);
         return !task.IsFaulted;
      }
   }
}
