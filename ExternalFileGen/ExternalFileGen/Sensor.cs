﻿namespace ExternalFileGen
{
   public enum Sensor
   {
      Temperature,
      Humidity,
      Co2,
      BirdWeight
   }
}
