﻿using CommandLine;

namespace ExternalFileGen
{
   interface IOptions
   {
      [Option('p', "period",
         Default = 60000,
         MetaValue = "ms",
         //Min = 100,
         Required = false,
         HelpText = "Millisecond period with which are the data generated (all sensors)")]
      int Period { get; set; }

      [Option('d', "directory",
         Default = @"C:\Logs\",
         Required = false,
         HelpText = "Output data directory")]
      string Path { get; set; }

      [Option('s', "separator",
         Default = ",",
         Required = false,
         HelpText = "Decimal separator for float numbers")]
      string Separator { get; set; }

      [Option('t', "temperature",
         Default = 1,
         //Min = 0,
         Required = false,
         HelpText = "Count of temperature sensors")]
      int SensorsTemp { get; set; }

      [Option('w', "humidity",
         Default = 1,
         //Min = 0,
         Required = false,
         HelpText = "Count of humidity sensors")]
      int SensorsHumidity
      {
         get; set;
      }

      [Option('c', "co2",
         Default = 1,
         //Min = 0,
         Required = false,
         HelpText = "Count of humidity sensors")]
      int SensorsCo2 { get; set; }

      [Option('b', "bird",
         Default = 1,
         //Min = 0,
         Required = false,
         HelpText = "Count of bird weight sensors")]
      int SensorsBirdWeight { get; set; }

      [Option('f', "dateformat",
         Default = "yyyy-MM-ddTHH:mm:ss.fff'Z'",
         Required = false,
         HelpText = "Format of date time to generate, see C# DateTime format")]
      string DateTimeFormat { get; set; }

   }

   [Verb("Opts", HelpText = "Generates data for BAT collector")]
   class Options : IOptions
   {
      public int Period { get; set; }
      public string Path { get; set; }
      public string Separator { get; set; }
      public int SensorsTemp { get; set; }
      public int SensorsHumidity { get; set; }
      public int SensorsCo2 { get; set; }
      public int SensorsBirdWeight { get; set; }
      public string DateTimeFormat { get; set; }
   }
}


